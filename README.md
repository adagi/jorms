jorms: a JavaScript Object-Relation Manager for MariaDB/MySQL
-------------------------------------------------------------

## Installation
```
npm install jorms --save
```

## Defining a model

```javascript
import { Model } from 'jorms';

export default class User extends Model {
  constructor() {
    super({
      // The name of the model. Must match the database's table
      // name. Will be the named used in the routing.
      modelName: 'user',

      // Maps column tables to JSON properties.
      attributes: {

      	// Attributes can be 'number', 'string', 'date' or 'boolean'.
        id: Model.attr('number'),
        name: Model.attr('string'),
        active: Model.attr('boolean'),

      	// A date can have a { format: 'DATE_MASK' } setting.
        createdOn: Model.attr('date', { format: 'yyyy-mm-dd'} ),

      }
    });
  }
}
```

## Defining the API router

```javascript
import { DatabaseRouter,
         MariaDBAdapter,
         EmberDataTransform } from 'jorms';

import User from './models/user';

export default class MyAppRouter extends DatabaseRouter {
  static run() {
    super.run({ 
      namespace: 'api',
      port: 8001,

      // An adapter allows the router to fetch data from
      // a database. Jorms includes built-in support for
      // MariaDB/MySQL databases.
      adapter: new MariaDBAdapter({
        host: '192.168.0.14',
        user: 'user',
        password: 'password',
        db: 'db',
        adapterSettings: {
          useSchema: true,
          tablePrefix: 't_'
        }
      }),

      // Register all models to expose via the API.
      models: [User],

      // Frameworks such as Ember Data expect a 'type'
      // property on every result returned in the API.
      // A Transform can do that. Transform objects allow
      // to manipulate the JSON payload before delivering
      // it to the requester.
      transform: EmberDataTransform
    });
  }
}

MyAppRouter.run();
```