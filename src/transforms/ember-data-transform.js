import Transform from '../transform';

export default class EmberDataTransform extends Transform {
  process(data, modelClass) {
    var result = super.process(data);
    let transform = this;

    result = result.map((model) => { 
      return transform.processSingle(model, modelClass, false);
    });

    return result;
  }
  processSingle(model, modelClass, avoidHash) {
    if (!avoidHash) {
      model = model.toHash();
    }
    model.type = modelClass._modelName;
    return model;
  }
}