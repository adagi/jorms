import moment from 'moment';

export default class Attribute {
  constructor(type, ...options) {
    this._attributeName = undefined;
    this._attributeType = type;
    if (type === 'date') {
      this._format = options.format;
    }
    this._rawValue = null;
  }
  get value() {
    if (this._attributeType === 'date') {
      let m = moment(this._rawValue);
      if (m._d.toString() === 'Invalid Date') {
        m = moment(this._rawValue, this._format);
        if (m._d.toString() === 'Invalid Date') {
          return this._rawValue;
        } else {
          return m;
        }
      } else {
        return m;
      }
    } else if (this._attributeType === 'number' || this._attributeType === 'boolean' || this._attributeType === 'string') {
      return this._rawValue;
    }
  }
  set value(val) {
    if (this._attributeType === 'date') {
      if (val._isAMomentObject) {
        this._rawValue = val;
        return;
      }
      if (this._format) {
        let m = moment(this._rawValue, this._format);
        if (m._d.toString() === 'Invalid Date') {
          throw new Error('Cannot set date ' + val + ' in attribute ' + this._attributeName);
        } else {
          this._rawValue = m;
        }
      } else {
        let m = moment(this._rawValue);
        if (m._d.toString() === 'Invalid Date') {
          throw new Error('Cannot set date ' + val + ' in attribute ' + this._attributeName);
        } else {
          this._rawValue = m;
        }
      }
    } else if (this._attributeType === 'number') {
      if (isNaN(val)) {
        throw new Error('Cannot set non-numeric value ' + val + ' in attribute ' + this._attributeName);
      } else {
        this._rawValue = val;
      }
    } else if (this._attributeType === 'boolean' || this._attributeType === 'string') {
      this._rawValue = val;
    }
  }
}