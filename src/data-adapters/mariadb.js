import Adapter from '../data-adapter';
import DatabaseClient from 'mariasql';
import DatabaseSerializer from '../data-serializers/mariadb';
import _ from 'incognito';
import RSVP from 'rsvp';

export default class MariaDBAdapter extends Adapter {
  constructor(options) {
    super(options);
    _(this).databaseOptions = options;
  }
  connect() {
    let privateData = _(this), _this = this;
    var db = new DatabaseClient();
    return new RSVP.Promise(function(resolve, reject) {
      db.connect({
        host: privateData.databaseOptions.host,
        user: privateData.databaseOptions.user,
        password: privateData.databaseOptions.password,
        db: privateData.databaseOptions.db
      });
      db.on('connect', function() {
        resolve(db);
      }).on('error', function(error) {
        reject(db);
      }).on('close', function(hadError) {
        if (hadError) {
          reject(db);
        } else {
          resolve();
        }
      })
    });
  }
  buildSQL(model, options) {
    var sql;
    let dbOptions = _(this).databaseOptions;
    let useSchema = dbOptions.adapterSettings.useSchema;
    let prefix = dbOptions.adapterSettings.tablePrefix;

    if (options.queryType === 'all') {
      sql = 'SELECT * FROM ';
      if (useSchema) {
        sql += dbOptions.db + '.';
      }
      if (prefix) {
        sql += prefix;
      }
      sql += model._pluralModelName;    
    } else if (options.queryType === 'id') {
      sql = 'SELECT * FROM ';
      if (useSchema) {
        sql += dbOptions.db + '.';
      }
      if (prefix) {
        sql += prefix;
      }
      sql += model._pluralModelName;
      sql += " WHERE id = :id";
    }
    return sql;
  }
  findAll(model) {
    let serializer = new DatabaseSerializer();
    let adapter = this;
    return adapter.connect().then(function(db) {
      return new RSVP.Promise(function(resolve, reject) {
        var sql = adapter.buildSQL(model, { queryType: 'all' });
        db.query(sql).on('result', function(result) {
          let rows = [];
          result.on('row', function(row) {
            rows.push(serializer.mapRow(new model.constructor(), row));
          });
          result.on('error', function(error) {
            reject(error);
          });
          result.on('end', function() {
            resolve(rows);
          })
        });
        db.end();
      });
    });
  }
  findById(model, id) {
    let serializer = new DatabaseSerializer();
    let adapter = this;
    return adapter.connect().then(function(db) {
      return new RSVP.Promise(function(resolve, reject) {
        var sql = adapter.buildSQL(model, { queryType: 'id' });
        var query = db.prepare(sql);
        db.query(query({ id: id })).on('result', function(result) {
          result.on('row', function(row) {
            resolve(serializer.mapRow(model, row));
          });
          result.on('error', function(error) {
            reject(error);
          });
        });
        db.end();
      });
    });
  }
}