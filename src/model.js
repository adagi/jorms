import Attribute from './attribute';
import inflect from 'inflection-extended';

export default class Model {
  constructor(options) {
    this._modelName = inflect.camelize(options.modelName);
    this._pluralModelName = options.pluralModelName || 
                            inflect.pluralize(this._modelName);
    this._attributes = options.attributes;
    this._adapter = undefined;
    for (let attrName in options.attributes) {
      let attr = options.attributes[attrName];
      attr._attributeName = attrName;
      Object.defineProperty(this, attrName, {
        get: function() {
          return attr.value;
        },
        set: function(val) {
          attr.value = val;
        }
      });
    }
  }
  findAll() {
    return this._adapter.findAll(this);
  }
  findById(id) {
    return this._adapter.findById(this, id);
  }
  query(filter) {

  }
  toHash() {
    let hash = {};
    for (let attrName in this._attributes) {
      hash[attrName] = this[attrName];
    }
    return hash;
  }
  static attr(type, ...options) {
    return new Attribute(type, options);
  }
}