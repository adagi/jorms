export default class Transform {
  process(data) {
    return data.map(this.processSingle);
  }
  processSingle(model) {
    return model.toHash();
  }
}