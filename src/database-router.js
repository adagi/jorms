import Router from 'omnirouter';
import JsonApiFormatter from 'jsonapi-formatter';

export default class DatabaseRouter extends Router {
  initialize(options) {
    let namespace = options.namespace ? ('/' + options.namespace) : '';
    this.get(namespace + '/', (request, response) => {
      response.ok();
    });
    let adapter = options.adapter;
    let transform = new options.transform();
    for (var modelObject of options.models) {
      let model = new modelObject();
      model._adapter = adapter;
      this.get(namespace + '/' + model._pluralModelName, (request, response) => {
        model.findAll().then(function(data) {
          response.ok(transform.process(data, model));
        });
      });
      this.get(namespace + '/' + model._modelName + '/:id', (request, response) => {
        model.findById(request.params.id).then(function(data) {
          response.ok(transform.processSingle(data, model));
        });
      });
    }
  }
  static run(options) {
    let router = new DatabaseRouter(options);
    router.use(JsonApiFormatter);
    router.listen(options.port, (error) => {
      console.log('Listening on http://localhost:' + options.port)
    });
  }
}