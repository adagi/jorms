import Serializer from '../data-serializer';
import inflect from 'inflection-extended';

export default class MariaDBSerializer extends Serializer {
  mapRow(modelClass, row) {
    let model = modelClass;
    for (var key in row) {
      model[inflect.camelize(key)] = row[key];
    }
    return model;
  }
}